# rasp

`rasp` (**ra**dio **s**ignal **p**ropgation) is a tool for estimating VHF/UHF radio (10MHz ~ 1GHz) propagation losses over irregular terrain.
`rasp` is written in [Python 3](https://www.python.org), accelerated by [numba](http://numba.pydata.org) and [numpy](https://numpy.org/).
